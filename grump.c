#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#define LINE_LENGTH 1000

void usage()
{
	printf("\n"
			"Useage:\n"
			"    grump <string> <filename>\n\n");
	return;
}

void grump(char* string, FILE* file)
{
	int lineno = 0;
	int isNewline = 1; // bool := is this next read on a new line?

	char* line;
	line = (char*)malloc(sizeof(char)*LINE_LENGTH);

	while(fgets(line, LINE_LENGTH, file)) {
		// Only increment line number if the last iteration found a '\n'
		if (isNewline) {
			++lineno;
			isNewline = 0;
		}

		if (strchr(line, '\n')) {
			isNewline = 1;
		}

		if (strstr(line, string)) {
			printf("%d: %s", lineno, line);
		}
	}
	printf("\n");
}

int main(int argc, char* argv[])
{
	FILE* fp;
	if (argc != 3 || !(fp = fopen(argv[2], "r"))) {
		usage();
		return 0;
	}

	grump(argv[1], fp);

	fclose(fp);
	return 0;
}

