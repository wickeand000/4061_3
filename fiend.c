#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <errno.h>
#include <limits.h>
#include <assert.h>

void usage()
{
	printf("\n"
			"Useage:\n"
			"    fiend [starting_dir] <filename>\n\n");
	return;
}

/****** These are not the functions you're looking for ******

   This is some queue code I threw together so that fiend
   could implement a breadth-first search just like `find'
   does. I like my `roll your own queues in C' like I like
   my women: poorly named and unevenly abstracted.

*************************************************************/
struct dir_list_elem{
	char* dir_name;
	struct dir_list_elem * next;
};

typedef struct dir_list_elem element;

int numdirs = 0;
int totaldirs = 0;
element* first = NULL;
element* last = NULL;

void enqueue(char* dir) {
	if (dir == NULL) {
		return;
	}
	element* obj;
	obj = (element*)malloc(sizeof(element));

	obj->dir_name = dir;
	obj->next = NULL;

	if (!first) {
		first = obj;
	}
	if (last) {
		last->next = obj;
	}

	last = obj;
	numdirs++;
	totaldirs++;
}

char* dequeue() {
	char* dir = NULL;
	if (first) {
		dir = first->dir_name;
		element* old_first = first;
		first = first->next;
		free(old_first);
		numdirs--;
	}
	if (!first) {
		last = NULL;
	}

	return dir;
}

/************************************************************/

void fiend(char* rel_path, char* search_term) {
	DIR* dirp;
	char* starting_path = realpath(rel_path, NULL);
	char temp_path[PATH_MAX];

	struct dirent* dent;
	struct stat s;
	struct stat* my_stat = &s;

	enqueue(starting_path);

	while(numdirs) {
		char* dir_path = dequeue();
		if (!(dirp = opendir(dir_path))) {
			continue;
		}
		while ((dent = readdir(dirp))) {

			// Skip dotfiles, just like `find'
			if (dent->d_name[0] == '.') {
				continue;
			}

			// Set up path name
			temp_path[0] = '\0'; // In the true spirit of C...
			strcat(temp_path, dir_path);
			strcat(temp_path, "/");
			strcat(temp_path, dent->d_name);
			stat(temp_path, my_stat);

			if (!(strcmp(dent->d_name, search_term))) {
				printf("%s\n", temp_path);
			}

			if (my_stat->st_mode & S_IFDIR) {
				char* x = realpath(temp_path, NULL);
				if (x) {
					enqueue(x);
				}
				else {
					//printf("could not traverse %s\n", temp_path);
				}
			}
		}
		closedir(dirp);
		free(dir_path);
	}
	printf("%d directories searched.\n", totaldirs - 7);
}

void testQueue() {
	dequeue();
	assert(first == NULL && last == NULL);
	enqueue("string one");
	assert(strstr(first->dir_name, "string one") &&
	       strstr(last->dir_name, "string one"));
	enqueue("string two");
	assert(strstr(first->dir_name, "string one") &&
	       strstr(last->dir_name, "string two"));
	dequeue();
	enqueue("string three");
	enqueue("string four");
	enqueue("string five");
	enqueue("string six");
	assert(strstr(first->dir_name, "string two") &&
	       strstr(last->dir_name, "string six"));
	dequeue();
	enqueue("string seven");
	dequeue();
	dequeue();
	dequeue();
	dequeue();
	assert(strstr(first->dir_name, "string seven") &&
	       strstr(last->dir_name, "string seven"));
	dequeue();
	assert(first == NULL && last == NULL);
}


int main(int argc, char* argv[])
{
	testQueue();

	DIR* dir;

	// Too many arguments
	if (2 > argc || argc > 3) {
		usage();
		return 0;
	}

	// Directory specified
	else if (argc == 3) {
		dir = opendir(argv[1]);
		if (dir) {
			closedir(dir);
			fiend(argv[1], argv[2]);
		}
		else {
			printf("problem opening %s", argv[1]);
		}
	}

	// Current directory assumed
	else if ((dir = opendir("."))) {
		closedir(dir);
		fiend(".", argv[1]);
	}

	// You dun screwed up
	else {
		usage();
	}
	return 0;
}

