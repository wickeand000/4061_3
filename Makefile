CC = gcc
CFLAGS = -Wall

EXES = grump losel fiend

%: %.c
	$(CC) $(CFLAGS) $< -o $@

all: $(EXES)

clean:
	rm -f $(EXES)
