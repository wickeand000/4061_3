#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <dirent.h>
#include <time.h>
#include <sys/stat.h>

void usage()
{
	printf("\n"
			"Useage:\n"
			"    losel [directory]\n\n");
	return;
}


void losel(char* path, DIR* dir) {
	// info containers
	int f_sz;
	char fullpath[200];
	char timebuf[15];
	struct stat s;

	// struct pointers
	struct dirent* dir_ent;
	struct stat* my_stat = &s;
	struct tm* my_time;

	printf("UID  GID  blks    size  modified      mode(UGO)  name\n");
	while ((dir_ent = readdir(dir))) {

		// Skip dotfiles, just like `ls -l'
		if (dir_ent->d_name[0] == '.') {
			continue;
		}

		// Set up path name and stat struct
		fullpath[0] = '\0'; // In the true spirit of C...
		strcat(fullpath, path);
		strcat(fullpath, "/");
		strcat(fullpath, dir_ent->d_name);
		stat(fullpath, my_stat);

		// UID, GID, blocks
		printf("%3d%5d%6d",
				(int)my_stat->st_uid,
				(int)my_stat->st_gid,
				(int)my_stat->st_blocks
			  );

		// size
		f_sz = my_stat->st_size;
		if (f_sz > 1000000000) {
			printf("%7.2fG", f_sz / 1000000000.0f);
		}
		else if (f_sz > 1000000) {
			printf("%7.2fM", f_sz / 1000000.0f);
		}
		else if (f_sz > 1000) {
			printf("%7.2fK", f_sz / 1000.0f);
		}
		else
			printf("%8d", f_sz);

		//modified
		//    Convert time_t to tm, because of course there is no
		//    strftime equivalent for time_t objects >:O
		my_time = localtime(&(my_stat->st_mtimespec.tv_sec));
		strftime(timebuf, 15, "%b %d, %H:%M", my_time);
		printf("%15s\t", timebuf);

		//permissions.
		printf("%4d", (my_stat->st_mode & S_IRWXU) >> 6);
		printf("%d", (my_stat->st_mode & S_IRWXG) >> 3);
		printf("%d   ", (my_stat->st_mode & S_IRWXO) >> 0);

		//name
		printf("%s", dir_ent->d_name);
		if (my_stat->st_mode & S_IFDIR) {
			printf("/");
		}

		printf("\n");
	}

	return;
}

int main(int argc, char* argv[])
{
	int opened = 0;
	DIR* dir;

	// Too many arguments
	if (argc > 2) {
		usage();
		return 0;
	}

	// Directory specified
	else if (argc == 2) {
		dir = opendir(argv[1]);
		if (dir) {
			opened = 1;
			losel(argv[1], dir);
		}
		else {
			printf("problem opening %s", argv[1]);
		}
	}

	// Current directory assumed
	else if ((dir = opendir("."))) {
		opened = 1;
		losel("./", dir);
	}

	// You dun screwed up
	else {
		usage();
	}

	if (opened) {
		closedir(dir);
	}
	return 0;
}

